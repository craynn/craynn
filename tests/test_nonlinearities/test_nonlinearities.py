import jax.numpy as jnp

def test_nonlineariries():
  from craynn import nonlinearities

  xs = jnp.linspace(-5, 5, num=129)

  for name in dir(nonlinearities):
    obj = getattr(nonlinearities, name)

    if isinstance(obj, type) and issubclass(obj, nonlinearities.Nonlinearity) and obj is not nonlinearities.Nonlinearity:
      try:
        f = obj()(xs)
      except Exception as e:
        raise ValueError(name) from e
