import os
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

import jax; jax.config.update('jax_platform_name', 'cpu')

import pytest

@pytest.fixture(scope='function')
def seed(request):
  import hashlib

  h = hashlib.sha256()
  h.update(bytes(request.function.__name__, encoding='utf-8'))
  digest = h.hexdigest()

  return int(digest[:8], 16)

@pytest.fixture(scope='function')
def rng(seed):
  import jax
  return jax.random.PRNGKey(seed)

@pytest.fixture(scope='function')
def cuda():
  import jax; jax.config.update('jax_platform_name', 'cuda')

  yield None

  jax.config.update('jax_platform_name', 'cpu')

def pytest_addoption(parser):
  parser.addoption(
    "--runslow", action="store_true", default=False, help="run slow tests"
  )


def pytest_configure(config):
  config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config, items):
  if config.getoption("--runslow"):
    # --runslow given in cli: do not skip slow tests
    return
  skip_slow = pytest.mark.skip(reason="need --runslow option to run")
  for item in items:
    if "slow" in item.keywords:
      item.add_marker(skip_slow)