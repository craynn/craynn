import jax
import jax.numpy as jnp
import numpy as np

def test_modes(rng):

  from craynn import network, dense, initialize_parameters

  net = network((None, 32))(
    dense(32), dense(2)
  )

  params = initialize_parameters(rng, net)

  rng, key = jax.random.split(rng)

  X = jax.random.normal(key, shape=(13, 32))

  assert net(X, params, rng=rng).shape == (13, 2)