import inspect

def test_meta_classes():

  class Meta(type):
    def __new__(cls, name, bases, dct):
      def f(self, x):
        return x + 1

      init = dct.pop('__init__', None)

      if init is not None:
        def __init__(self, *args, **kwargs):
          print('Remember!', init)
          self.args = args
          self.kwargs = kwargs

          init(self, *args, **kwargs)

        return super().__new__(cls, name, bases, {**dct, 'f': f, '__init__': __init__})
      else:
        return super().__new__(cls, name, bases, {**dct, 'f': f})

  class MetaPropagator(object, metaclass=Meta):
    pass

  class F(MetaPropagator):
    def __init__(self, z):
      self.z = z
      
      super().__init__()

  f = F(1)
  print(f.f(1))

  print(f.args, f.kwargs)
