import jax
import jax.numpy as jnp
import numpy as np

import craynn


def test_init(rng):
  from craynn import network, train, get_output, get_all_outputs, linear
  from craynn import dense, flatten

  net = network((None, 10))(
    dense(128), dense(64), dense(32), dense(6), dense(1, activation=linear()), flatten(1)
  )

  params = net.initialize(rng)

  rng, key_X, key_norm = jax.random.split(rng, num=3)
  X = jax.random.normal(key_X, shape=(128, 10))

  predictions_before = get_all_outputs(net.outputs(), substitutes={**dict(zip(net.inputs(), [X])), **params})

  input_layer, = net.inputs()
  params = train.normalize_parameters(
    key_norm, net, {input_layer : X}, params, iterations=128, learning_rate=0.1, batch=32
  )

  predictions = get_all_outputs(net.outputs(), substitutes={**dict(zip(net.inputs(), [X])), **params})
  for layer in predictions:
    if isinstance(layer, craynn.Parameter):
      continue

    pred_before = predictions_before[layer]
    pred = predictions[layer]

    print(type(layer).__name__)
    print('Means')
    print(jnp.mean(pred_before, axis=0).tolist())
    print(jnp.mean(pred, axis=0).tolist())
    print('Stds')
    print(jnp.std(pred_before, axis=0).tolist())
    print(jnp.std(pred, axis=0).tolist())

def check_reduction(psig, pshape, osig, oshape):
  from craynn.train.common import get_reduction

  p = jnp.ones(shape=pshape)
  y = jnp.ones(shape=oshape)

  common = set(psig) & set(osig)

  raxes, perm, broadcast = get_reduction(psig, pshape, osig, oshape)
  assert len(raxes) == len(oshape) - len(common)

  assert len(broadcast) == p.ndim

  reduced = jnp.sum(y, axis=raxes)
  assert len(perm) == reduced.ndim
  aligned = jnp.transpose(reduced, perm)
  broadcasted = aligned[broadcast]

  p_ = p + broadcasted
  assert p.shape == p_.shape

def test_reduction():
  check_reduction('IO', (13, 5), 'O', (17, 5))
  check_reduction('IAB', (13, 5, 3), 'BA', (17, 3, 5))
  check_reduction('', (3, 5, 7), '', (9, 11,))

  check_reduction('IO', (13, 5), 'O', (7, 17, 5))

  try:
    check_reduction('IAB', (13, 5, 3), 'BA', (17, 5, 3))
  except ValueError:
    pass

def test_normalize(rng):
  from craynn import network, conv, conv_pool, flatten

  encoder = network((None, 1, 28, 28))(
    conv(16), conv(24), conv_pool(),
    conv(32), conv(48), conv_pool(),
    conv(64), conv_pool(),

    flatten()
  )
  input_layer, = encoder.inputs()

  key_init, key_X, key_norm = jax.random.split(rng, num=3)

  X = jax.random.normal(key_X, shape=(5, 1, 28, 28))

  params = craynn.train.initialize_parameters(key_init, encoder)
  params = craynn.train.normalize_parameters(
    key_norm,
    encoder, {input_layer: X}, params
  )

def test_normalize2(rng):
  from craynn import network, dense, general_mean_pool

  encoder = network((None, None, 5))(
    dense(7), general_mean_pool(axis=(1, )),
  )
  input_layer, = encoder.inputs()

  key_init, key_X, key_norm = jax.random.split(rng, num=3)

  X = jax.random.normal(key_X, shape=(13, 11, 5))

  params = craynn.train.initialize_parameters(key_init, encoder)
  params = craynn.train.normalize_parameters(
    key_norm,
    encoder, {input_layer: X}, params
  )

