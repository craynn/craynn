import jax

def test_dense(seed, ):
  from craynn import layers, dag, train

  rng = jax.random.PRNGKey(seed)

  input = layers.InputLayer(shape=(None, 32))
  dense1 = layers.dense(32)(input)
  dense2 = layers.dense(2)(dense1)

  params = train.initialize_parameters(rng, dense2)

  def f(X):
    return dag.get_output(dense2, substitutes={ input : X, **params })

  y = f(jax.random.normal(rng, shape=(5, 32)))

  assert y.shape == (5, 2)
