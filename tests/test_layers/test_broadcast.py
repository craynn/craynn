import jax
import jax.numpy as jnp

import numpy as np

def test_broadcast_concat(seed):
  from craynn.dag import get_output
  from craynn.layers import expand_concat, InputLayer, broadcast

  rng = jax.random.PRNGKey(seed)

  in1 = InputLayer((None, None, 3))
  in2 = InputLayer((None, 1))

  extend2 = broadcast[:, None, :](in2)
  concat = expand_concat()(in1, extend2)

  key_1, key_2 = jax.random.split(rng, num=2)

  v1 = jax.random.normal(key_1, shape=(15, 7, 3))
  v2 = jax.random.normal(key_2, shape=(15, 1))

  result = get_output(concat, substitutes={in1 : v1, in2 : v2})

  print()
  print(jnp.shape(result))

  assert np.allclose(result[:, :, :3], v1)

  for i in range(7):
    assert np.allclose(result[:, i, -1], v2[:, 0])