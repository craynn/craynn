import pytest

import jax
import jax.numpy as jnp

@pytest.mark.slow
def test_conv_formats(seed, cuda):
  import time
  n_iter = 32
  rng = jax.random.PRNGKey(seed)

  key_X, key_W = jax.random.split(rng)
  w, h = 129, 127
  fw, fh = 32, 32
  cin, cout = 32, 64
  n = 9
  X = jax.random.normal(key_X, shape=(n, cin, w, h), dtype=jnp.float32)

  conv_spec = ('NCHW', 'OIHW', 'NCHW')
  W = jax.random.normal(key_W, shape=(cout, cin, fw, fh), dtype=jnp.float32)
  dimension_numbers = jax.lax.conv_dimension_numbers(X.shape, W.shape, conv_spec)

  @jax.jit
  def conv_oi(X, W):
    conved = jax.lax.conv_general_dilated(
      X, W, window_strides=(1, 1), padding=((0, 0), (0, 0)),
      dimension_numbers=dimension_numbers
    )

    return jnp.sum(jnp.square(conved))

  grad_conv_oi = jax.jit(jax.grad(conv_oi, argnums=-1))

  ### trigger compilation
  _ = conv_oi(X, W)

  start_time = time.perf_counter_ns()
  for _ in range(n_iter):
    _ = conv_oi(X, W)
  total = time.perf_counter_ns() - start_time

  print('OI format, iterations per second: %.1lf' % ((10**9) * n_iter / total, ))

  _ = grad_conv_oi(X, W)

  start_time = time.perf_counter_ns()
  for _ in range(n_iter):
    _ = grad_conv_oi(X, W)
  total = time.perf_counter_ns() - start_time

  print('Grad OI format, iterations per second: %.1lf' % ((10 ** 9) * n_iter / total,))

  conv_spec = ('NCHW', 'IOHW', 'NCHW')
  W = jax.random.normal(key_W, shape=(cin, cout, fw, fh), dtype=jnp.float32)
  dimension_numbers = jax.lax.conv_dimension_numbers(X.shape, W.shape, conv_spec)

  @jax.jit
  def conv_io(X, W):
    conved = jax.lax.conv_general_dilated(
      X, W, window_strides=(1, 1), padding=((0, 0), (0, 0)),
      dimension_numbers=dimension_numbers
    )

    return jnp.sum(jnp.square(conved))

  grad_conv_io = jax.jit(jax.grad(conv_io, argnums=-1))

  ### trigger compilation
  _ = conv_io(X, W)

  start_time = time.perf_counter_ns()
  for _ in range(n_iter):
    _ = conv_io(X, W)
  total = time.perf_counter_ns() - start_time

  print('IO format, iterations per second: %.1lf' % ((10 ** 9) * n_iter / total,))

  ### trigger compilation
  _ = grad_conv_io(X, W)

  start_time = time.perf_counter_ns()
  for _ in range(n_iter):
    _ = grad_conv_io(X, W)
  total = time.perf_counter_ns() - start_time

  print('Grad IO format, iterations per second: %.1lf' % ((10 ** 9) * n_iter / total,))

@pytest.mark.slow
def test_conv_shapes():
  from craynn.layers.conv_ops import conv_utils

  for n in [3, 4, 7, 15]:
    X = jnp.zeros(shape=(1, 2, n, n))

    for k in range(1, 6):
      for s in range(1, 4):
        for p in ['SAME', 'VALID', 0, 1, 2, 3, 4, 5]:
          for d in range(1, 4):
            W = jnp.ones(shape=(3, 2, k, k))

            C = jax.lax.conv_general_dilated(
              X, W, window_strides=(s, s),
              padding=((p, p), (p, p)) if isinstance(p, int) else p,
              rhs_dilation=(d, d)
            )

            t = conv_utils.conv_output_shape(
              X.shape, kernel_shape=(3, 2, k, k), strides=(s, s), dilation=(d, d),
              padding=conv_utils.get_padding(p, ndim=2) if isinstance(p, int) else p,
              conv_spec=conv_utils.get_conv_spec(2)
            )

            if C.shape[-1] > 0:
              assert C.shape == t, '%dx%d, k=%s, s=%d, p=%s, d=%d: %s vs %s' % (n, n, k, s, p, d, t, C.shape[2:])

def test_deconv(seed, ):
  import numpy as np

  def deconv1(input, W, pad, stride, dilation):
    pad_left, pad_right = pad

    input = np.concatenate([
      np.zeros(pad_left),
      input,
      np.zeros(pad_right)
    ], axis=0)

    if dilation > 1:
      W_ = np.zeros(shape=(dilation * (W.shape[0] - 1) + 1, ))
      W_[::dilation] = W
      W = W_

    n, = input.shape
    k, = W.shape

    output = np.zeros(shape=(n * stride + k - stride, ))

    for i in range(n):
      output[i * stride:i * stride + k] += W * input[i]

    return output

  from craynn.layers.conv_ops.conv_utils import conv_transpose, conv_transposed_output_shape

  rng = np.random.RandomState(seed)

  for _ in range(1):
    n = rng.randint(2, 17)
    k = rng.randint(1, 5)
    s = rng.randint(1, k + 1)
    d = rng.randint(1, 4)
    p = rng.randint(0, 20)

    input = rng.normal(size=(n,)).astype(np.float64)
    W = rng.normal(size=(k,)).astype(np.float64)

    conv_spec = ('NCH', 'OIH', 'NCH')
    dimension_numbers = jax.lax.conv_dimension_numbers(
      (1, 1, *input.shape), (1, 1, *W.shape),
      conv_spec
    )

    print()
    print(input.shape)
    print(W.shape)
    d1 = deconv1(input, W, pad=(p, p), stride=s, dilation=d)
    d2 = conv_transpose(
      input.reshape((1, 1, -1)),
      W[::-1].reshape((1, 1, -1)),
      strides=(s,),
      padding=((p, p), ),
      dimension_numbers=dimension_numbers,
      feature_group_count=1,
      dilation=(d, )
    )[0, 0]

    shape = conv_transposed_output_shape(
      (1, 1, *input.shape),
      (1, 1, *W.shape),
      strides=(s, ),
      padding=((p, p),),
      conv_spec=conv_spec,
      dilation=(d, )
    )

    print(d1.shape)

    assert d1.shape == d2.shape, \
      f'Input: {input.shape}, W: {W.shape}, stride={s}, dilation={d}, pad={p}\n' \
      f'{d1}\n' \
      f'{d2}'

    assert shape[2:] == d1.shape, \
      f'Input: {input.shape}, W: {W.shape}, stride={s}, dilation={d}, pad={p}\n' \
      f'{d1}\n' \
      f'{d2}'

    assert np.allclose(d1, d2, atol=1e-3), f'{np.max(np.abs(d1 - d2))}'

def test_conv(rng):
  from craynn import layers, train
  from craynn import dag

  input = layers.InputLayer(shape=(None, 1, 32, 32))
  l1 = layers.conv(32)(input)
  l2 = layers.conv(2)(l1)

  params = train.initialize_parameters(rng, l2)

  def f(X):
    return dag.get_output(l2, substitutes={ input : X, **params})

  y = f(jax.random.normal(rng, shape=(5, 1, 32, 32)))

  assert y.shape == (5, 2, 28, 28)