import numpy as np

def test_parameters(rng,):
  from craynn import dag
  from craynn.parameters import ones_init, glorot_normal_init, decomposition, get_all_free_parameters, const_init

  W = ones_init(trainable=False)(shape=(32, 32), trainable=True)
  assert not W.properties['trainable']

  W = ones_init(trainable=True)(shape=(32, 32), trainable=False)
  assert W.properties['trainable']

  W = ones_init()(shape=(32, 32), trainable=True)
  assert W.properties['trainable']

  W = ones_init()(shape=(32, 32), trainable=False)
  assert not W.properties['trainable']

  def get(parameter):
    return dag.get_output(parameter, common_kwargs={'rng': rng})

  assert get(W).shape == (32, 32)
  assert np.allclose(get(W), np.ones(shape=(32, 32)))

  W = glorot_normal_init(gain=1.0)(shape=(32, 32), trainable=True)
  assert get(W).shape == (32, 32)

  W = decomposition(n=5, w1=ones_init(), w2=ones_init())(shape=(32, 32))

  assert W.properties['composite']
  assert 'composite' not in W.w1.properties
  assert 'composite' not in W.w2.properties

  assert get(W).shape == (32, 32)
  w1, w2 = W.incoming
  assert np.allclose(get(w1), np.ones(shape=(32, 5)))
  assert np.allclose(get(w2), np.ones(shape=(5, 32)))
  assert len(get_all_free_parameters(W)) == 2

  W = decomposition(n=5)(shape=(32, 32))

  W = decomposition(n=5, w1=ones_init(composite=True, flag=False), w2=ones_init(composite=True, flag=True))(shape=(32, 32))
  assert W.properties['composite']
  assert W.w1.properties['composite']
  assert W.w2.properties['composite']

  assert 'flag' not in W.properties
  assert not W.w1.properties['flag']
  assert W.w2.properties['flag']

  W = const_init(1)((32, 15))
  assert get(W).shape == (32, 15)
  assert np.all(np.abs(get(W) - 1) < 1e-6)

  W = const_init(value=1)((32, 15))
  assert get(W).shape == (32, 15)
  assert np.all(np.abs(get(W) - 1) < 1e-6)

  W = const_init(value=1)(shape=(32, 15))
  assert get(W).shape == (32, 15)
  assert np.all(np.abs(get(W) - 1) < 1e-6)

  W = const_init(value=1, prop=True)(shape=(32, 15), prop=False)
  assert get(W).shape == (32, 15)
  assert np.all(np.abs(get(W) - 1) < 1e-6)
  assert W.properties['prop']