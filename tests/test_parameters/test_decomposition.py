import jax
import jax.random

def test_decomposition_params(seed):
  from craynn import network, dense, decomposition, linear, train

  net = network((None, 12))(
    dense(32, activation=linear(), W=decomposition(n=5))
  )

  rng = jax.random.PRNGKey(seed)

  key_init, key_x = jax.random.split(rng, num=2)
  params = train.initialize_parameters(key_init, net)
  X = jax.random.normal(key_x, shape=(13, 12))

  print(net(X, params).shape)

  for k, param in params.items():
    print(k)
