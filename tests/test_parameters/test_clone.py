import jax
import jax.numpy as jnp

def test_clone(rng, ):
  from craynn.parameters import shared_parameter, glorot_uniform_init
  from craynn import dag, train

  W = shared_parameter(glorot_uniform_init(gain=5))

  W1 = W(shape=(10, 15))

  try:
    W_ = W(shape=(9, 15))
  except AssertionError:
    pass
  else:
    raise Exception('Exception should be raised!')


  W2 = W(shape=(10, 15))
  W3 = W(shape=(10, 15))

  def get(parameter, **modes):
    return dag.get_output(parameter, common_kwargs=modes)

  params = train.initialize_parameters(rng, (W1, W2, W3))

  assert len(params) == 1

  X1, X2, X3 = dag.get_output((W1, W2, W3), substitutes=params)

  assert jnp.allclose(X1, X2) and jnp.allclose(X1, X3)

